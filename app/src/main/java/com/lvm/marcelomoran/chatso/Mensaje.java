package com.lvm.marcelomoran.chatso;

import android.os.AsyncTask;

import java.io.PrintWriter;

public class Mensaje extends AsyncTask<Void, Void, Void>
{
    private String message;
    PrintWriter m_print_writer;

    // Constructor para cuando se quiere enviar un mensaje de texto.
    public Mensaje(String message, PrintWriter printWriter)
    {
        this.message = message; // Se concatena el primer carácter identificador con el texto.
        m_print_writer = printWriter;
    }

    // Enviar el texto y nos aseguramos que los datos pendientes se envien al target con el flush.
    @Override
    protected Void doInBackground(Void... params)
    {
        m_print_writer.write(this.message);
        m_print_writer.flush();
        return null;
    }

    @Override
    protected void onPostExecute(Void result)
    {
        m_print_writer.flush();
    }
}
