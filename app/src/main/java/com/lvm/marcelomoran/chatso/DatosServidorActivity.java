package com.lvm.marcelomoran.chatso;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DatosServidorActivity extends AppCompatActivity
{
    public static final String EXTRA_ALIAS_CLIENTE = Cliente.EXTRA_ALIAS_CLIENTE;

    private void i_abrirChatCliente(Button button, final EditText edit_text_ip, final EditText edit_text_puerto, final String alias_cliente)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                String ip, puerto;

                ip = edit_text_ip.getText().toString();
                puerto = edit_text_puerto.getText().toString();

                if (ip.equals("") != true && puerto.equals("") != true)
                {
                    Intent intent;

                    intent = new Intent(DatosServidorActivity.this, Cliente.class);

                    intent.putExtra(Cliente.EXTRA_IP, ip);
                    intent.putExtra(Cliente.EXTRA_PUERTO, puerto);
                    intent.putExtra(Cliente.EXTRA_ALIAS_CLIENTE, alias_cliente);

                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "los campos de textos tienen que tener datos", Toast.LENGTH_SHORT);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_servidor);

        EditText edit_text_ip;
        EditText edit_text_puerto;
        Button button_conectar;
        String alias_cliente;

        edit_text_ip = (EditText) findViewById(R.id.ads_editTextIP);
        edit_text_puerto = (EditText) findViewById(R.id.ads_editTextPuerto);

        alias_cliente = "";

        if (savedInstanceState == null)
        {
            Bundle extras = getIntent().getExtras();

            if (extras != null)
            {
                alias_cliente = extras.getString(EXTRA_ALIAS_CLIENTE);
            }
            else
            {
                alias_cliente = "Cliente";
            }
        }

        button_conectar = (Button) findViewById(R.id.ads_buttonConectar);
        i_abrirChatCliente(button_conectar, edit_text_ip, edit_text_puerto, alias_cliente);

    }
}
