package com.lvm.marcelomoran.chatso;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor extends AppCompatActivity
{
    private final int SLEEP_TIME = 100; // Tiempo de espera para el bucle principal de los hilos mientras hay conexión.

    public static final String EXTRA_ALIAS_SERVIDOR = "ALIAS_SERVIDOR";
    private String alias_servidor;

    Thread socketServerThread;  // Hilo que espera a las llamadas de un cliente.
    ServerSocket serverSocket;  // Socket servidor que espera a aceptar una conexión en un puerto.

    Socket threadServerSocket = null;    // Sockets que permiten la comunización entre cliente y servidor.

    private Seguridad seguridad;

    // Elementos de la interfaz gráfica.
    TextView tvChat;
    ScrollView scrollView;
    Button btnEnviar;
    EditText etMensaje;

    PrintWriter printWriter = null;     // Permite escribir información en el socket, que más tarde será leída.

    // --------------------------------------------------------------

    private void i_escribirMensajeChat(String mensaje)
    {
        tvChat.append(mensaje);
        scrollView.fullScroll(View.FOCUS_DOWN);
    }

    // --------------------------------------------------------------

    private void i_escribirMensajeEnviadoPantalla(String mensaje_a_enviar)
    {
        String mensaje_chat;

        mensaje_chat = "Servidor: " + mensaje_a_enviar + "\n";
        i_escribirMensajeChat(mensaje_chat);
    }

    // --------------------------------------------------------------

    private void i_enviarMensaje(String mensaje_a_enviar)
    {
        String mensaje_cifrado;

        try
        {
            mensaje_cifrado = seguridad.encriptar(mensaje_a_enviar);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            mensaje_cifrado = mensaje_a_enviar;
        }

        Mensaje sender = new Mensaje(mensaje_cifrado, printWriter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            sender.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else
        {
            sender.execute();
        }

        etMensaje.setText("");
    }

    // --------------------------------------------------------------

    private void i_buttonEnviarMensaje(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                String mensaje_a_enviar;

                mensaje_a_enviar = etMensaje.getText().toString();

                if (mensaje_a_enviar.equals("") != true)
                {
                    if (threadServerSocket != null && threadServerSocket.isConnected())
                    {
                        i_escribirMensajeEnviadoPantalla(mensaje_a_enviar);
                        i_enviarMensaje(mensaje_a_enviar);
                    }
                }
            }
        });
    }

    private void i_inicializarVistas()
    {
        tvChat = (TextView)findViewById(R.id.tvChat);
        scrollView = (ScrollView)findViewById(R.id.scrollView);

        btnEnviar = (Button)findViewById(R.id.btnEnviar);
        i_buttonEnviarMensaje(btnEnviar);

        etMensaje = (EditText)findViewById(R.id.etMensaje);
    }

    private void i_abrirHiloServidor()
    {
        socketServerThread = new Thread(new SocketServerThread());
        socketServerThread.start();
    }

    private void i_obtener_alias_usuario(Bundle savedInstanceState)
    {
        if (savedInstanceState == null)
        {
            Bundle extras = getIntent().getExtras();

            if (extras != null)
            {
                alias_servidor = extras.getString(EXTRA_ALIAS_SERVIDOR);
            }
            else
            {
                alias_servidor = "Servidor";
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        seguridad = new Seguridad();

        i_inicializarVistas();

        i_abrirHiloServidor();

        i_obtener_alias_usuario(savedInstanceState);
    }

    private void i_cerrarHilosConexiones()
    {
        // Al abandonar la aplicación se cierran todos los sockets e hilos que aún existan.
        try
        {
            if (serverSocket != null)
            {
                serverSocket.close();
            }
            if (threadServerSocket != null)
            {
                threadServerSocket.close();
            }
            if (socketServerThread != null)
            {
                socketServerThread.interrupt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        i_cerrarHilosConexiones();
    }

    // --------------------------------------------------------------

    private void i_escribirMensajeRecibidoPantalla(String mensaje_recibido)
    {
        String mensaje_chat;

        mensaje_chat = "Cliente: " + mensaje_recibido + "\n";
        i_escribirMensajeChat(mensaje_chat);
    }

    private class SocketServerThread extends Thread
    {
        static final int SocketServerPORT = 8080;   // El servidor siempre escucha en este puerto.
        Boolean serverDisconnect = false;     // Flags para controlar si hay que realizar conexiones.
        String mensaje_recibido;
        String mensaje_recibido_descifrado;

        @Override
        public void run()
        {
            try
            {
                serverSocket = new ServerSocket();  // Se crea el socket...
                serverSocket.setReuseAddress(true); // Se marca como reutilizable por si se quedase zombie...
                serverSocket.bind(new InetSocketAddress(SocketServerPORT));

                while (true)
                {
                    final String notificacion_conexion;

                    serverDisconnect = false;

                    try
                    {
                        threadServerSocket = serverSocket.accept(); // El servidor espera a que le llegue una conexión...
                    }
                    catch (Exception e)
                    {
                        threadServerSocket = null;
                        break;
                    }

                    notificacion_conexion = threadServerSocket.getInetAddress() + " se ha conectado." + "\n";

                    Servidor.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            i_escribirMensajeChat(notificacion_conexion);
                        }
                    });

                    // Se prepara para escuchar mensajes...
                    InputStreamReader inputStreamReader = new InputStreamReader(threadServerSocket.getInputStream());
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    // ... y para escribirlos.
                    printWriter = new PrintWriter(threadServerSocket.getOutputStream(), true);

                    while (threadServerSocket.isConnected())
                    { // Mientras la conexion sigue en pie

                        if (bufferedReader.ready())
                        {
                            mensaje_recibido = bufferedReader.readLine(); // Se lee la línea...

                            try
                            {
                                mensaje_recibido_descifrado = seguridad.desencriptar(mensaje_recibido);
                            }
                            catch (Exception e)
                            {
                                mensaje_recibido_descifrado = mensaje_recibido;
                            }

                            runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                { // ... y se escribe en el chat.
                                    i_escribirMensajeRecibidoPantalla(mensaje_recibido_descifrado);
                                    mensaje_recibido = "";
                                    mensaje_recibido_descifrado = "";
                                }
                            });
                        }

                        // Para que no se compruebe excesivamente, lo que puede ocasionar cuelgues, se espera durante cierto tiempo antes de cada nueva iteración.
                        try
                        {
                            Thread.sleep(SLEEP_TIME);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }

                }
            }
            catch (IOException e)
            {
                tvChat.append("No se ha podido crear el socket");
                e.printStackTrace();
            } finally
            {
                // Aquí se gestiona la desconexión del servidor, y volver a poner bien los botones.
                serverDisconnect = false;
                if (threadServerSocket != null)
                {
                    try
                    {
                        threadServerSocket.close();
                        threadServerSocket = null;
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }

    }
}
