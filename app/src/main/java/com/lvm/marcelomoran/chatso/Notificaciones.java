package com.lvm.marcelomoran.chatso;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import static android.app.Notification.DEFAULT_LIGHTS;
import static android.app.Notification.DEFAULT_SOUND;
import static android.app.Notification.DEFAULT_VIBRATE;

/**
 * Created by RosaDiez on 20/11/2017.
 */

public class Notificaciones
{
    private static final int i_NOTIFICACION_ID = 1;
    Context m_context;

    NotificationCompat.Builder mbuilder;
    NotificationManager mnotification_manager;
    int m_numero_mensajes_nuevos = 0;

    // --------------------------------------------------------------

    public Notificaciones(Context context)
    {
        this.m_context = context;
    }

    // --------------------------------------------------------------

    private String i_textoNotificacion()
    {
        String texto;

        texto = "Mensajes nuevos: " + m_numero_mensajes_nuevos;

        return texto;
    }

    // --------------------------------------------------------------

    public void nuevaNotificacion()
    {
        if (mbuilder == null)
        {
            m_numero_mensajes_nuevos = 1;

            mbuilder = new NotificationCompat.Builder(m_context).setTicker(i_textoNotificacion()).setContentTitle(i_textoNotificacion()).setContentText("Nuevo mensaje").setSmallIcon(R.mipmap.ic_launcher).setAutoCancel(true);
            mbuilder.setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE | DEFAULT_LIGHTS);

            mnotification_manager = (NotificationManager) m_context.getSystemService(Context.NOTIFICATION_SERVICE);
            mnotification_manager.notify(i_NOTIFICACION_ID, mbuilder.build());
        }
        else
        {
            mbuilder.setTicker(i_textoNotificacion()).setContentTitle(i_textoNotificacion()).setNumber(m_numero_mensajes_nuevos);
            mnotification_manager.notify(i_NOTIFICACION_ID, mbuilder.build());
            m_numero_mensajes_nuevos++;
        }
    }

    // --------------------------------------------------------------

    public void borrarNotificaciones()
    {
        m_numero_mensajes_nuevos = 0;
        mnotification_manager.cancel(i_NOTIFICACION_ID);
    }
}
