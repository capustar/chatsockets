package com.lvm.marcelomoran.chatso;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity
{
    // --------------------------------------------------------------

    private void i_abrirServidor(Button button, final EditText edit_text_alias)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                String alias_usuario;

                alias_usuario = edit_text_alias.getText().toString();

                if (alias_usuario.equals("") != true)
                {
                    Intent intent;

                    intent = new Intent(getApplicationContext(), Servidor.class);
                    intent.putExtra(alias_usuario, Servidor.EXTRA_ALIAS_SERVIDOR);
                    startActivity(intent);
                }
            }
        });
    }

    // --------------------------------------------------------------

    private void i_abrirCliente(Button button, final EditText edit_text_alias)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                String alias_usuario;

                alias_usuario = edit_text_alias.getText().toString();

                if (alias_usuario.equals("") != true)
                {
                    Intent intent;

                    intent = new Intent(MainActivity.this, DatosServidorActivity.class);
                    intent.putExtra(alias_usuario, DatosServidorActivity.EXTRA_ALIAS_CLIENTE);
                    startActivity(intent);
                }
            }
        });
    }

    // --------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText edit_text_alias;
        Button button_servidor;
        Button button_cliente;

        edit_text_alias = (EditText) findViewById(R.id.ma_editTextAlias);

        button_servidor = (Button) findViewById(R.id.ma_buttonServidor);
        i_abrirServidor(button_servidor, edit_text_alias);

        button_cliente = (Button) findViewById(R.id.ma_buttonCliente);
        i_abrirCliente(button_cliente, edit_text_alias);
    }
}
