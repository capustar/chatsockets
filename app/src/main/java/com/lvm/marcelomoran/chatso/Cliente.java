package com.lvm.marcelomoran.chatso;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by RosaDiez on 19/11/2017.
 */

public class Cliente extends AppCompatActivity
{
    public static final String EXTRA_IP = "IP";
    public static final String EXTRA_PUERTO = "PUERTO";

    public static final String EXTRA_ALIAS_CLIENTE = "ALIAS_CLIENTE ";
    private String alias_cliente;

    private Seguridad seguridad;

    private final int SLEEP_TIME = 100; // Tiempo de espera para el bucle principal de los hilos mientras hay conexión.

    Socket threadClientSocket = null;    // Sockets que permiten la comunización entre cliente y servidor.
    PrintWriter printWriter = null;     // Permite escribir información en el socket, que más tarde será leída.

    // Elementos de la interfaz gráfica.
    TextView tvChat;
    ScrollView scrollView;
    Button btnEnviar;
    EditText etMensaje;

    // --------------------------------------------------------------

    private void i_escribirMensajeChat(String mensaje)
    {
        tvChat.append(mensaje);
        scrollView.fullScroll(View.FOCUS_DOWN);
    }

    // --------------------------------------------------------------

    private void i_escribirMensajeEnviadoPantalla(String mensaje_a_enviar)
    {
        String mensaje_chat;

        mensaje_chat = alias_cliente + ": " + mensaje_a_enviar + "\n";
        i_escribirMensajeChat(mensaje_chat);
    }

    // --------------------------------------------------------------

    private void i_enviarMensaje(String mensaje_a_enviar)
    {
        String mensaje_cifrado;

        try
        {
            mensaje_cifrado = seguridad.encriptar(mensaje_a_enviar);
        } catch (Exception e)
        {
            e.printStackTrace();
            mensaje_cifrado = mensaje_a_enviar;
        }

        Mensaje sender = new Mensaje(mensaje_cifrado, printWriter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            sender.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else
        {
            sender.execute();
        }

        etMensaje.setText("");
    }

    // --------------------------------------------------------------

    private void i_buttonEnviarMensaje(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                String mensaje_a_enviar;

                mensaje_a_enviar = etMensaje.getText().toString();

                if (mensaje_a_enviar.equals("") != true)
                {
                    if (threadClientSocket != null && threadClientSocket.isConnected())
                    {
                        i_escribirMensajeEnviadoPantalla(mensaje_a_enviar);
                        i_enviarMensaje(mensaje_a_enviar);
                    }
                }
            }
        });
    }

    private void i_inicializarVistas()
    {
        tvChat = (TextView)findViewById(R.id.tvChat);
        scrollView = (ScrollView)findViewById(R.id.scrollView);

        btnEnviar = (Button)findViewById(R.id.btnEnviar);
        i_buttonEnviarMensaje(btnEnviar);

        etMensaje = (EditText)findViewById(R.id.etMensaje);
    }

    private void i_conectarse_al_servidor(Bundle savedInstanceState)
    {
        // Se crea el hilo del cliente

        if (savedInstanceState == null)
        {
            Bundle extras = getIntent().getExtras();

            if (extras != null)
            {
                String ip_servidor, puerto_servidor;
                ip_servidor = extras.getString(EXTRA_IP);
                puerto_servidor = extras.getString(EXTRA_PUERTO);

                MyClientTask myClientTask = new MyClientTask(ip_servidor, Integer.parseInt(puerto_servidor));
                myClientTask.execute();
            }
        }
    }

    private void i_obtener_alias_usuario(Bundle savedInstanceState)
    {
        if (savedInstanceState == null)
        {
            Bundle extras = getIntent().getExtras();

            if (extras != null)
            {
                alias_cliente = extras.getString(EXTRA_ALIAS_CLIENTE);
            }
            else
            {
                alias_cliente = "Cliente";
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        seguridad = new Seguridad();

        i_inicializarVistas();

        i_conectarse_al_servidor(savedInstanceState);

        i_obtener_alias_usuario(savedInstanceState);
    }

    private void i_cerrarHilosConexiones()
    {
        try
        {
            if (threadClientSocket != null)
            {
                threadClientSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        i_cerrarHilosConexiones();
    }

    public class MyClientTask extends AsyncTask<Void, Void, Void>
    {
        // Variables que guardan la ip, el puerto y la respuesta
        String dstAddress;
        int dstPort;
        String mensaje_recibido;
        String mensaje_recibido_descifrado;

        MyClientTask(String addr, int port)
        {
            // Constructor para inicializar ip y puerto
            dstAddress = addr;
            dstPort = port;
        }

        // --------------------------------------------------------------

        private void i_escribirMensajeRecibidoPantalla(String mensaje_recibido)
        {
            String mensaje_chat;

            mensaje_chat = "Servidor: " + mensaje_recibido + "\n";
            i_escribirMensajeChat(mensaje_chat);
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            threadClientSocket = null;
            try
            { // Una vez inicializado, se crea el socket del cliente y se avisa de la conexion
                threadClientSocket = new Socket(dstAddress, dstPort);

                if (threadClientSocket.isConnected())
                {
                    final String notificacion_conexion;

                    notificacion_conexion = "Te has conectado al servidor " + dstAddress + "\n";

                    Cliente.this.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            i_escribirMensajeChat(notificacion_conexion);
                        }
                    });
                }

                // Se prepara para escuchar mensajes...
                InputStreamReader inputStreamReader = new InputStreamReader(threadClientSocket.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                // ... y para escribirlos.
                printWriter = new PrintWriter(threadClientSocket.getOutputStream(), true);

                // Mientras la conexion sigue en pie
                while (threadClientSocket.isConnected())
                {
                    if (bufferedReader.ready())
                    {
                        mensaje_recibido = bufferedReader.readLine(); // Se lee la línea...

                        try
                        {
                            mensaje_recibido_descifrado = seguridad.desencriptar(mensaje_recibido);
                        }
                        catch (Exception e)
                        {
                            mensaje_recibido_descifrado = mensaje_recibido;
                        }

                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            { // ... y se escribe en el chat.
                                i_escribirMensajeRecibidoPantalla(mensaje_recibido_descifrado);
                                mensaje_recibido = "";
                                mensaje_recibido_descifrado = "";
                            }
                        });
                    }

                    // Para que no se compruebe excesivamente, lo que puede ocasionar cuelgues, se espera durante cierto tiempo antes de cada nueva iteración.
                    try
                    {
                        Thread.sleep(SLEEP_TIME);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }

            }
            catch (UnknownHostException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            finally
            {
                if (threadClientSocket != null)
                {
                    try
                    {
                        threadClientSocket.close();
                        threadClientSocket = null;
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }
    }
}
